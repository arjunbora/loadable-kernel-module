obj-m += lkm.o
all:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules
clean:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean
	rm -rf active_dump.csv inactive_dump.csv libraries_count.csv result.csv processes_vs_libraries.png
