#include<linux/module.h>
#include<linux/kernel.h>
#include<linux/mmzone.h>
//#include<arch/ia64/include/asm/numa.h>
#include<asm/numa.h>
#include<linux/mm.h>
#include "lkm.h"
#include <linux/memcontrol.h>
#include <linux/slab.h>
#define lru_to_page(_head) (list_entry((_head)->prev, struct page, lru))
#define LIMIT 200000

int write_file(struct file *ofilp, void *buf, int len)
{
	mm_segment_t oldfs;
	int written_bytes;

	oldfs = get_fs();
	set_fs(KERNEL_DS);
	written_bytes = vfs_write(ofilp, buf, len, &ofilp->f_pos);
	set_fs(oldfs);
	return written_bytes;
}

int dump(unsigned long *old, unsigned long *counter, int *written_bytes, char *buf, unsigned long *new, unsigned long *pages_scanned, struct file *filp)
{
	memset(buf, '\0', PAGE_SIZE);
	if(*old != 0) {
		sprintf(buf, "%lu,%lu\n", *old, *counter);
        	*written_bytes = write_file(filp, buf, strlen(buf));
	}
        memset(buf, '\0', PAGE_SIZE);
        *old = *new;
        *pages_scanned += *counter;
        *counter = 1;
	if (*written_bytes < 0) {
        	pr_err("Write to file failed.\n");
                return 1;
        }
	return 0;
}

int helper4(struct lruvec *lruvec, struct scan_control *sc, isolate_mode_t mode, struct file *filp, struct file *filp2)	//isolate lru page
{
	struct list_head *src;
	struct address_space *mapping;
	struct inode *inode;
	unsigned long old=0;
	unsigned long new=0;
	struct page *page, *next;
	char *buf = NULL;
	int written_bytes = 0;
	unsigned long counter = 1;
	unsigned long control = 0;
	unsigned long pages_scanned = 0;	

	buf = kmalloc(PAGE_SIZE, GFP_KERNEL);
	src = &lruvec->lists[3];
	printk("printing active list\n");
	list_for_each_entry_safe(page, next, src, lru) {
		if (control>LIMIT) {
			printk("reached the active limit\n");
			if(dump(&old, &counter, &written_bytes, buf, &new, &pages_scanned, filp))
				goto out;
			break;
		}
		control++;
		if(!page) {
			pr_err("Page is NULL.\n");
			if(dump(&old, &counter, &written_bytes, buf, &new, &pages_scanned, filp))
                                goto out;
                        continue;
		}
		mapping = page->mapping;
		if(!mapping) {
			pr_err("mapping is NULL. flag -%lu-\n", page->flags);
			if(dump(&old, &counter, &written_bytes, buf, &new, &pages_scanned, filp))
                                goto out;
                        continue;
		}
		inode = mapping->host;
		if(!inode) {
			pr_err("inode is NULL.\n");
			if(dump(&old, &counter, &written_bytes, buf, &new, &pages_scanned, filp))
                                goto out;
			continue;
		}
		new = inode->i_ino;
		/*if(new <= 10) {
			//printk("inode no is NULL.\n");
			pages_scanned++;
			continue;
		}*/
		if(old != new){
			if(dump(&old, &counter, &written_bytes, buf, &new, &pages_scanned, filp))
                                goto out;
		} else
			counter++;
	}
	pages_scanned += counter;
	if(dump(&old, &counter, &written_bytes, buf, &new, &pages_scanned, filp))
	        goto out;
	printk("total %lu active inodes written\n", control);

	counter = 1;
	control = 0;
	old = 0;
	new = 0;
	src = &lruvec->lists[2];
	printk("printing inactive list\n");
	list_for_each_entry_safe(page, next, src, lru) {
		if (control>LIMIT) {
			printk("reached the inactive limit\n");
			if(dump(&old, &counter, &written_bytes, buf, &new, &pages_scanned, filp2))
                                goto out;
			break;
		}
		control++;
        	if(!page) {
			pr_err("Page is NULL.\n");
			if(dump(&old, &counter, &written_bytes, buf, &new, &pages_scanned, filp2))
                        	goto out;
			continue;
		}
		mapping = page->mapping;
		if(!mapping) {
			pr_err("mapping is NULL. flag -%lu-\n", page->flags);
			if(dump(&old, &counter, &written_bytes, buf, &new, &pages_scanned, filp2))
                                goto out;
			continue;
		}
		inode = mapping->host;
		if(!inode) {
			pr_err("inode is NULL.\n");
			if(dump(&old, &counter, &written_bytes, buf, &new, &pages_scanned, filp2))
                                goto out;
			continue;
		}
		new = inode->i_ino;
		/*if(new <= 10) {
			//printk("inode no is NULL.\n");
			pages_scanned++;
			continue;
		}*/
        	if(old != new) {
			if(dump(&old, &counter, &written_bytes, buf, &new, &pages_scanned, filp2))
                                goto out;
		} else
			counter++;
    	}
	pages_scanned += counter;
	if(dump(&old, &counter, &written_bytes, buf, &new, &pages_scanned, filp2))
        	goto out;
	printk("total %lu inactive inodes written\n", control);
out:
	kfree(buf);
	return pages_scanned;
}

int helper3(struct lruvec *lruvec, int swappiness, struct scan_control *sc, struct file *filp, struct file *filp2)//shrink_lruvec() + //shrink_list() + shrink_active_list(nr_to_scan, lruvec, sc, lru);
{
	//struct zone *zone = lruvec_zone(lruvec);	//good function to know
	isolate_mode_t isolate_mode = 0;
	return helper4(lruvec, sc, isolate_mode, filp, filp2);
}

int helper2(struct zone *zone, struct scan_control *sc, bool is_classzone, struct file *filp, struct file *filp2)
{
	struct mem_cgroup *root = sc->target_mem_cgroup;
	struct mem_cgroup_reclaim_cookie reclaim = {
		.zone = zone,
		.priority = sc->priority,
	};
	struct mem_cgroup *memcg;
	struct lruvec *lruvec;
	int swappiness;

	memcg = mem_cgroup_iter(root, NULL, &reclaim);
	if(!memcg) {
		printk("memcg is NULL!!!\n");
		return -1;
	}
	lruvec = mem_cgroup_zone_lruvec(zone, memcg);
	if(!lruvec) {
        printk("lruvec is NULL!!!\n");
        return -1;
	}
	swappiness = 60;
	return helper3(lruvec, swappiness, sc, filp, filp2);
}
int helper(struct zonelist *zonelist, struct scan_control *sc)
{
	struct zone *zone;
	struct zoneref *z;
    	enum zone_type requested_highidx;
	enum zone_type classzone_idx;
	unsigned long pages_scanned = 0;
	struct file *filp = filp_open("active_dump.csv", O_WRONLY | O_CREAT | O_APPEND, 0666);
	struct file *filp2 = filp_open("inactive_dump.csv", O_WRONLY | O_CREAT | O_APPEND, 0666);
	char *buf = NULL;
	int written_bytes = 0;

        if (!filp || IS_ERR(filp)) {
                pr_err("Output file open failed - %d\n", (int) PTR_ERR(filp));
                goto out;
        }
        filp->f_pos = 0;
        if (!filp2 || IS_ERR(filp2)) {
                pr_err("Output file open failed - %d\n", (int) PTR_ERR(filp2));
                goto out;
        }
        filp2->f_pos = 0;

	buf = kzalloc(PAGE_SIZE, GFP_KERNEL);
        sprintf(buf, "inode, number of contiguous pages\n");
        written_bytes = write_file(filp, buf, strlen(buf));	//check error message!!
        written_bytes = write_file(filp2, buf, strlen(buf));
        memset(buf, '\0', PAGE_SIZE);
	kfree(buf);

	sc->nr_scanned = 0;
	requested_highidx = gfp_zone(sc->gfp_mask);
	classzone_idx = requested_highidx;
    	for_each_zone_zonelist_nodemask(zone, z, zonelist, requested_highidx, sc->nodemask) {
		if(!zone  || IS_ERR(zone)) {
			printk("zone is NULL!!!\n");
			goto out;
		}
		pages_scanned = helper2(zone, sc, zone_idx(zone) == classzone_idx, filp, filp2);
		printk("pages_sacanned=%lu-\n", pages_scanned);
    	}
out:
	if (filp && !IS_ERR(filp))
                filp_close(filp, NULL);
        if (filp2 && !IS_ERR(filp2))
                filp_close(filp2, NULL);
	return 0;
}

int init_module(void)
{
	struct scan_control sc = {
		.nr_to_reclaim = 1000000,
		.gfp_mask = GFP_HIGHUSER_MOVABLE,
		.priority = DEF_PRIORITY,
		.may_writepage = 1,
		.may_unmap = 1,
		.may_swap = 1,
		.hibernation_mode = 1,
	};
	struct zonelist *zonelist;

	printk("\n\n");
	zonelist = node_zonelist(numa_node_id(), sc.gfp_mask);
	if(!zonelist || IS_ERR(zonelist)) {
		printk("zonelist returned null/error\n");
		return 0;
	}
	return helper(zonelist, &sc);
}

void cleanup_module(void)
{
    printk("Bye....\n");
}
