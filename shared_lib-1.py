#export MPLBACKEND=Agg
import matplotlib.pyplot as plt
import pandas as pd
import subprocess
import sys
import matplotlib
import csv
#from pprint import pprint

from matplotlib import pyplot as plt
filename = "libraries_count.csv"
target = open(filename, 'w', 0)
f1 = open('./result.csv', 'w+')
#output = open('./refined_dump.csv', 'w+')
target.write("count,libraries,inode,size\n")
command = 'cat /proc/*/maps | awk \'{print $6}\' | grep \'\.so\' | sort | uniq | xargs lsof  | awk \'{if(NR>1) print $9 "," $8 "," $7}\' | sort | uniq -c | sort -n | awk \'{print $1 "," $2}\''
output,error  = subprocess.Popen(command, universal_newlines=True, shell=True, stdout=target).communicate()

libraries_and_count = pd.read_csv(filename)
count_shared_libs = libraries_and_count[libraries_and_count['count']>1]
libraries_and_count['count'].hist(bins=max(libraries_and_count['count'])) # change this line if want to see only shared libraries
plt.ylabel('Number of shared libraries')
plt.xlabel('Number of processes')
plt.savefig('processes_vs_libraries.png')
plt.close("all")

print "Total libraries in memory  - "+str(libraries_and_count['count'].count())
print "Libraries shared by 1+ processes - "+str(count_shared_libs['count'].count())

active = open('active_dump.csv').read()
inactive = open('inactive_dump.csv').read()
total_diff = 0

active_dict = dict()
with open('active_dump.csv', 'rb') as csvfile:
	inode_pagecount = csv.reader(csvfile)
	inode_pagecount.next()
	for row in inode_pagecount:
#		print row
		inode = int(row[0])
		pagecount = int(row[1])
		try:
			active_dict[inode] += pagecount
		except KeyError:
			active_dict[inode] = pagecount

inactive_dict = dict()
with open('inactive_dump.csv', 'rb') as csvfile:
        inode_pagecount = csv.reader(csvfile)
        inode_pagecount.next()
	for row in inode_pagecount:
#               print row
		inode = int(row[0])
                pagecount = int(row[1])
                try:
                        inactive_dict[inode] += pagecount
                except KeyError:
                        inactive_dict[inode] = pagecount
#print "Inode->Pages_In_Memory Mapping for inactive pages list:"
#print >>output, "inode, active/inactive, rss(total_pages_in_one_of_the_list x 4KB)\n"
print >>f1, "Inode,Library,Active Pages,Inactive Pages,Binary Size(KB),RSS(KB),Diff"
for inode in libraries_and_count['inode']:
	lib = libraries_and_count[libraries_and_count.inode == inode]['libraries'].values[0]
	count = libraries_and_count[libraries_and_count.inode == inode]['count'].astype(int).values[0]
	size = libraries_and_count[libraries_and_count.inode == inode]['size'].astype(int).values[0]
	rss = 0
	active_pages = 0
	inactive_pages = 0
	found = 0
	inode_str = str(inode)	
	
	if inode in active_dict:
		active_pages = active_dict[inode]
		rss += active_pages*4096
		found = 1
		#print >>output, inode_str + ",active,"+ str(active_dict[inode]*4096)
	
	if inode in inactive_dict:
		inactive_pages = inactive_dict[inode]
		rss += inactive_pages*4096
		found = 1
		#print >>output, inode_str + ",inactive,"+ str(inactive_dict[inode]*4096)

	if(found==1):
		diff = size-rss
	else:
		diff = 0
#	print diff
	total_diff += diff
#	if inode in active_dict:d
#		print inode, active_dict[inode]
#	saved_size = (count-1)*size
#	total_saved += saved_size
	if inode in active_dict:
		print >>f1, inode_str + "," + lib + "," + str(active_pages) + "," + str(inactive_pages) + "," + str(size/1024) + "," + str(rss/1024) + "," + str(diff/1024)
	elif inode in inactive_dict:
		print >>f1, inode_str + "," + lib + "," + str(active_pages) + "," + str(inactive_pages) + "," + str(size/1024) + "," + str(rss/1024) + "," + str(diff/1024)
	else:
		print >>f1, inode_str + "," + lib + "BUG,BUG,BUG,BUG,BUG"

print >>f1, ",,,,,," + str(total_diff/(1024*1024))+"MB"

